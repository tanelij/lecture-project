import { describe, it, expect } from "vitest";
import { render, screen } from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import { vi } from 'vitest'
import App from '../src/App'


function initMock(item, isDone){
    const fetchMock = vi.fn(() => (
        {
            json: async () => {
                return [{id: 1, item, isDone}]
            }
        }
    ));
      
    vi.stubGlobal('fetch', fetchMock);
}

initMock();

describe('Todos list', () => {
    it('is empty on startup', () => {
        render(<App />)
        const items = document.querySelectorAll('.item')
        expect(items).toHaveLength(0)
    })
})

describe('Create first todo', () => {
    it('add single item', async () => {
        initMock("get help", false);
        render(<App />)
        const user = userEvent.setup()
        const input = screen.getAllByRole('textbox')[0]
        const button = screen.getByRole('button')

        await user.type(input, "get help")
        await user.click(button)

        const items = document.querySelectorAll('.item')
        expect(items).toHaveLength(1)
        expect(items[0]).toHaveTextContent("get help")
    })
})



describe('Debugger', () => {
    it('should print debugging info', () => {
        render(<App />)
        screen.debug()
    })
})


describe('something truthy and falsy', () => {
    it('true to be true', () => {
        expect(true).toBe(true)
    })

    it('false to be false', () => {
        expect(false).toBe(false)
    })
})