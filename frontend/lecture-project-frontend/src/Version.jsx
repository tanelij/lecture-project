import { useEffect, useState } from 'react'
import './App.css'

function App() {
  const [version, setVersion] = useState();
  useEffect(() => {
    (async () => {
      try {
        const request = await fetch("/api/version");
        const text = await request.text();
        setVersion(text);
      } catch (e) {
        setVersion("no version");
      }
    })();

  }, [])
  return (
    <div style={{ position: "absolute", bottom: "0" }}>Version number: {version}</div>
  )
}

export default App
