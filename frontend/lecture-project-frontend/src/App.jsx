import { useState, useEffect } from 'react'
import { BiTrash } from 'react-icons/bi'
import Version from './Version.jsx'
import './App.css'

const App = () => {
    const [list, setList] = useState([])
    const [input, setInput] = useState('')

    useEffect(() => getItems, [])

    const getItems = async () => {
        const request = await fetch("/api/");
        const todosArr = await request.json();
        setList(todosArr)
    }

    const addItem = async () => {
        if (!input) return null

        const rawResponse = await fetch('/api/', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ item: input })
        });
        const updatedList = await rawResponse.json();

        console.log(updatedList)
        setList(updatedList)
        setInput('')
    }

    const toggle = (id) => async () => {
        const targetItem = list.find(item => item.id === id);

        const rawResponse = await fetch('/api/' + id, {
            method: 'PUT',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ isDone: !targetItem.isDone })
        });
        const updatedList = await rawResponse.json();
        console.log(updatedList)
        setList(updatedList)
    }

    const remove = (id) => async () => {
        const rawResponse = await fetch('/api/' + id, {
            method: 'DELETE',
        })
        const updatedList = await rawResponse.json();

        console.log(updatedList)
        setList(updatedList)
        // setList(list.filter(item => item.id !== id))
    }

    return <div className='App'>

        <input value={input} onChange={(event) => setInput(event.target.value)} />
        <button onClick={addItem}>Add</button>

        {list.map(({ id, isDone, item }) => {
            return <div key={id} className='item'>
                <span className={isDone ? 'done' : ''}
                    onClick={toggle(id)}>
                    {item}
                </span>
                <BiTrash className='icon' onClick={remove(id)} />
            </div>
        })}

        <Version></Version>
    </div>
}

export default App
