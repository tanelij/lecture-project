import express from "express";
import { Router } from "express";
import todoRoute from "./todo.js";

const PORT = process.env.PORT || 3000;
const server = express();


const router = Router();
router.get("/version", (req, res) => {
    res.send("1.1");
});

server.use("/api", router);
server.use("/", express.static("./backend/dist"));
server.use("/api", todoRoute);

server.listen(PORT, () => {
    console.log(`Server listening on port: ${PORT}`);
})