import express, {Router} from "express";

const router = Router();
router.use(express.json());
const todos = new Map();
let idCounter = 1;

function addTodo(isDone = false, item){
    const id = idCounter.toString();
    const obj = {
        id,
        item,
        isDone
    }
    todos.set(id, obj);
    idCounter++;
    return obj;
}

function updateTodo(id, {isDone, item}){
    const todo = todos.get(id);

    if(isDone !== undefined){
        todo.isDone = isDone;
    }
    if(item !== undefined){
        todo.item = item;
    }

}

addTodo(false, "test");

function getTodos(){
    return Array.from(todos.values());
}


router.get("/", (req, res) => {
    res.json(getTodos())
});

router.get("/:id", (req, res) => {
    const todo = todos.get(req.params.id);

    if(todo === undefined){
        return res.status(404).send();
    }
    res.json(todo);
})

router.post("/", (req, res) => {
    const {isDone, item} = req.body;
    const todo = addTodo(isDone, item);
    res.json(getTodos());
})

router.delete("/:id", (req, res) => {
    if(!todos.delete(req.params.id)){
        return res.status(404).send();
    }
    res.json(getTodos());
});

router.put("/:id", (req, res) => {
    if(!todos.has(req.params.id)){
        return res.status(404).send();
    }
    updateTodo(req.params.id, req.body);
    res.json(getTodos());
});

export default router;